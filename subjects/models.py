from django.db import models


class Subject(models.Model):
    """Subjects model."""
    name = models.CharField(max_length=128)

    def __str__(self):
        return self.name


class Student(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=128)
    enrollment = models.BooleanField(default=False)
    Subject = models.ManyToManyField(Subject)

    
    def __str__(self):
        return self.name

