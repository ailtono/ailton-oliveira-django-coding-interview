from rest_framework.viewsets import ViewSet
from serializers import StudentSerializer, SubjectsSerializer
from models import Student
# Create your views here.

class StudentView(ViewSet):
    serializer_class = StudentSerializer

    def get(self,request):
        student_id = request.GET.get('id')
        return Student.objects.filter(id=student_id).first()
    
    def post(self,request):
        enrollment_flag = bool(request.POST.get('status'))
        student_id = request.GET.get('id')
        student_instance = Student.objects.filter(id=student_id).first()
        student_instance.enrollment = enrollment_flag
        student_instance.save()

class SubjecView(ViewSet):
    serializer_class = SubjectsSerializer

    def get(self,request):
        subject_id = request.GET.get('id')
        return Student.objects.filter(id=subject_id).first()

