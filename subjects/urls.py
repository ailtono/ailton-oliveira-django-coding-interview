from django.urls import path
from subjects.views import StudentView

urlpatterns = [
    path('admin/',StudentView, name='Student view'),
]
